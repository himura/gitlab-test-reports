﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace GitLabTestReports {
    public static class XmlGenerator {
        static XmlWriterSettings xmlWriterSettings = new XmlWriterSettings {
            Indent = true,
            OmitXmlDeclaration = true
        };
        static XmlSerializerNamespaces xmlSerializerNamespaces = new XmlSerializerNamespaces(
            new[] { XmlQualifiedName.Empty }
        );

        public static void Write<T>(T report, string outputPath) {
            using(var stream = new StreamWriter(outputPath))
            using(var writer = XmlWriter.Create(stream, xmlWriterSettings)) {
                new XmlSerializer(typeof(T)).Serialize(writer, report, xmlSerializerNamespaces);
            }
        }

        public static void Save(this GitLabReport report, string outputPath) =>
            Write(report, outputPath);
    }
}
