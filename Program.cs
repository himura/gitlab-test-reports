namespace GitLabTestReports {
    class Program {
        static void Main(string[] args) {
            new GitLabReport(
                new TestCase() {
                    Name = "Name 1"
                },
                new TestCase() {
                    Suite = "Suite 2",
                    Name = "Name 2",
                    Filename = "Program.cs#L11",
                    Status = new TestError("multiline\ndescription"),
                    DurationSeconds = 2
                },
                new TestCase() {
                    Suite = "Suite 3",
                    Name = "Name 3",
                    Filename = "Program.cs#L18",
                    Status = new TestFailure("another\nmultiline\ndescription"),
                    DurationSeconds = 3
                },
                new TestCase() {
                    Suite = "Suite 4",
                    Name = "Name 4",
                    Filename = "example-report.xml",
                    Status = new TestSkipped(),
                    DurationSeconds = 4
                }
            ).Save(args[0]);
        }
    }
}
