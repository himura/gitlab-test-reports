﻿using System;
using System.Xml.Serialization;

namespace GitLabTestReports {

    [Serializable()]
    [XmlRoot("testsuite")]
    public class GitLabReport {

        [XmlElement("testcase")]
        public TestCase[] TestCases;

        public GitLabReport() { }
        public GitLabReport(params TestCase[] testCases) {
            TestCases = testCases;
        }
    }


    [Serializable()]
    public class TestCase {

        [XmlAttribute("classname")]
        public string Suite;

        [XmlAttribute("name")]
        public string Name;

        [XmlAttribute("file")]
        public string Filename;

        [XmlAttribute("time")]
        public decimal DurationSeconds;

        [XmlElement("error", typeof(TestError))]
        [XmlElement("failure", typeof(TestFailure))]
        [XmlElement("skipped", typeof(TestSkipped))]
        public object Status;
    }


    [Serializable()]
    public class TestError {

        [XmlText()]
        public string Value;

        public TestError() { }
        public TestError(string output) {
            Value = output;
        }
    }

    [Serializable()]
    public class TestFailure {

        [XmlText()]
        public string Value;

        public TestFailure() { }
        public TestFailure(string output) {
            Value = output;
        }
    }

    [Serializable()]
    public class TestSkipped { }
}
